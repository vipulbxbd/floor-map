

function getDistanceFromLatLonInMeters(loc1,loc2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(loc2.lat-loc1.lat);  // deg2rad below
  var dLon = deg2rad(loc2.lng-loc1.lng);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(loc1.lat)) * Math.cos(deg2rad(loc2.lat)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c *1000; // Distance in meters
//  console.log("distance is : "+ d);
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function translateImageRatioToDistance(storeWidth, storeLength, imageWidth, imageLength) {



}

function mapOriginOfImageToStore() {



}

function coordinatesNeededToPlaceOnImage() {


}

function getDistanceBetweenTwoCoordinates(cor1,cor2){
	//console.log("coordinate of x,y :"+cor2.x, cor2.y)
    return Math.sqrt((cor1.x-cor2.x)*(cor1.x-cor2.x) + (cor1.y-cor2.y)*(cor1.y-cor2.y))
}
function mapPixcelDistanceToActulaDistance(pixcelDistance, actualDistance){

  return (actualDistance/pixcelDistance)
}

function getTheta(side1,side2,angleSide){
  //console.log("calculate " ,side1*side1 + side2*side2 - angleSide*angleSide)
  return Math.acos((side1*side1 + side2*side2 - angleSide*angleSide)/(2*side1*side2))
}

var distance = {
		returnCoordinatesForLocations: function(locations, metadata) {

			  var originLocation = metadata.locations.originLocation
			  var otherLocation = metadata.locations.otherLocation
			  // var gp3 = [51.296985, 6.831874]
      //  console.log(otherLocation);
        //console.log(locations );
			  var originCoordinate = metadata.coordinates.originCoordinate
			  var otherCoordinate = metadata.coordinates.otherCoordinate
			  var disGp1_Gp2 = getDistanceFromLatLonInMeters(originLocation,otherLocation)
			  var pDisBetweenpp1andpp2 = getDistanceBetweenTwoCoordinates(originCoordinate,otherCoordinate)
			  var ratio = pDisBetweenpp1andpp2/disGp1_Gp2
			//  console.log(pDisBetweenpp1andpp2)
			 // console.log("location data : " + locations)
			  for (var index = 0;index< locations.length; index++){
  				  var location = locations[index].location
  			//	  console.log("location data : " + location.lat)
  				  var distanceFromOrigin = getDistanceFromLatLonInMeters(originLocation,location)
  				  var distanceFromGp2 = getDistanceFromLatLonInMeters(otherLocation,location)
  				  var pixDistanceFromOrign = ratio*distanceFromOrigin
  				  var pDisFromgP2 = ratio*distanceFromGp2
  				  var theta = getTheta(pDisBetweenpp1andpp2, pixDistanceFromOrign, pDisFromgP2)
  				//  console.log("theta ", theta)
  				  var x = pixDistanceFromOrign*(Math.cos(theta))
  				  var y = pixDistanceFromOrign*(Math.sin(theta))
            locations[index].coordinate = {"x" : x + metadata.coordinates.originCoordinate.x, "y" : y + metadata.coordinates.originCoordinate.y}
			  }
			  return locations

		}
}

//6045, 8203
//var metadata = {
//		"locations": {
//			"originLocation": {
//				"lat": 51.295728,
//				"lng": 6.831035
//			},
//			"otherLocation": {
//				"lat": 51.295919,
//				"lng": 6.832304
//			}
//		},
//		"coordinates": {
//			"originCoordinate": {
//				"x": 0,
//				"y": 0
//			},
//			"otherCoordinate": {
//				"x": 656,
//				"y": 0
//			}
//		}
//
//	}
//
//console.log(distance.returnCoordinatesForLocations([{"lat":51.296685,"lng":6.830979}], metadata))
module.exports = distance;
// getDistanceFromLatLonInMeters(51.028347, 7.014035,51.027197, 7.015583)
//function showDots(pointsX , pointsY) {
//for(var i = 0; i < pointsX.length; i++)
//{
//    var div = document.createElement('div');
//    div.className = 'dot';
//    div.style.left = pointsX[i] + 'px';
//    div.style.bottom = pointsY[i] + 'px';
//    document.getElementById('wrapper').appendChild(div);
//}
//
//}
//function plotDots(){
//
//  var point = returnActualPixcelForLatlng(51.296685, 6.830979)
//  console.log(point.x + " **** " + point.y)
//  showDots([point.x], [point.y])
//}
//
// console.log(returnActualPixcelForLatlng(51.296685, 6.830979))
//plotDots()
