'use strict';


var distance = require('./distance.js')
var data_manager = require('./data-manager')
// Your first function handler

function prepareCoordinatesToReturnFromBody(body){
  var locations = body.payload
  var mapId = body.mapId
  var metadata = data_manager.fetchDataForMapId(mapId)
  var points = distance.returnCoordinatesForLocations(locations, metadata)
  return {"mapId": mapId, "imageInfo":metadata.imageinfo, "payload": points}
}

module.exports.getCoordinates = (event, context, cb) => cb(null, prepareCoordinatesToReturnFromBody(event.body)
);

// You can add more handlers here, and reference them in serverless.yml
