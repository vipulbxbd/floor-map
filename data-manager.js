/**
 * http://usejsdoc.org/
 */

var data_manager = {

	fetchDataForMapId: function (id){

	var metadata = [{
		 "mapId":1,
			"imageinfo":{
				"size": {
					"height":1024,
					"width":654
				}
			},
			"locations": {
				"originLocation": {
					"lat": 51.295728,
					"lng": 6.831035
				},
				"otherLocation": {
					"lat": 51.295919,
					"lng": 6.832304
				}
			},
			"coordinates": {
				"originCoordinate": {
					"x": 0,
					"y": 0
				},
				"otherCoordinate": {
					"x": 656,
					"y": 0
				}
			}

		},
		{
			"mapId":2,
			"imageinfo":{
				"size": {
					"height":1024,
					"width":654
				}
			},
				"locations": {
					"originLocation": {
						"lat": 51.027081,
						"lng": 7.014209
					},
					"otherLocation": {
						"lat": 51.027151,
						"lng": 7.015592
					}
				},
				"coordinates": {
					"originCoordinate": {
						"x": 613,
						"y": 232
					},
					"otherCoordinate": {
						"x": 6079,
						"y": 150
					}
				}

			}
	]
	for (var i = 0; i < metadata.length; i++) {
		if (metadata[i].mapId == id) {
			return metadata[id]
		}
		return null;
	}

	}
}

module.exports = data_manager;
