


var express = require('express');
var app = express();
var distance = require('./distance.js')
var data_manager = require('./data-manager')

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/getCoordinates', function (req, res) {
	var locations = req.body.payload
  var mapId = req.body.mapId
  var metadata = data_manager.fetchDataForMapId(mapId)
  var points = distance.returnCoordinatesForLocations(locations, metadata)
  res.send({"mapId": mapId, "imageInfo":metadata.imageinfo, "payload": points});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
